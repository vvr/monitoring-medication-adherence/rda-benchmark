"""
MIT License

Copyright (c) 2022 Visualization and Virtual Reality Lab

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

"""

"""
This file annotates _a0 dataset to match the format of _f1,_a1 and _g1. It is executed only once and is not needed if 
_a0/annotation.csv already exists
"""


import numpy as np
# import tensorflow as tf
# from tensorflow.keras.utils import to_categorical
# from tensorflow.keras.layers.advanced_activations import ELU
# import keras.backend.tensorflow_backend as ktf
from sklearn.model_selection import KFold
from sklearn.metrics import confusion_matrix
import random
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from config import *

from sklearn.model_selection import cross_val_predict
from sklearn.mixture import GaussianMixture
import glob
import os
import wave
import scipy.io.wavfile as wv

def getClassFromFilename(fnm):
    if 'Drug' in fnm:
        cls='Drug'
    if 'Exhale' in fnm:
        cls='Exhale'
    if 'Inhale' in fnm:
        cls = 'Inhale'
    if 'Noise' in fnm:
        cls = 'Noise'
    return cls



batch_size = 64
epochs = 40
classes=['Drug','Exhale','Inhale','Noise']

num_classes = len(classes)
trainedModel = './trained_models/inhaler_model_5.h5py'
fromScratch=False
doStore=False
path="./"

np.set_printoptions(formatter={'float': lambda x: "{0:0.3f}".format(x)})
a0 = glob.glob(os.path.join(path + '_a0/', '*.wav'))
annotationFilePathToWrite=path + '_a0/'+"annotation.csv"
fobject = open(annotationFilePathToWrite, 'w')
fobject.close()
fobject = open(annotationFilePathToWrite, 'a')
for fnmpath in a0:
    fnmpath=fnmpath.replace("\\", "/")
    fnm=fnmpath.split(sep="/")[-1]
    cls=getClassFromFilename(fnm)

    w = wv.read(fnmpath)
    w_ = wave.openfp(fnmpath)
    maxval = 2 ** ((w_.getsampwidth() * (8) - 1))
    fs = w[0]
    audioData = w[1]
    audioData = audioData / maxval
    startA=0
    stopA=len(audioData)-1
    fobject.write(fnm+","+cls+","+str(startA)+","+str(stopA)+"\n")


fobject.close()